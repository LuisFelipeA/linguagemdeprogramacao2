<?php

class Registro {
    public $Entrada;
    public $Saida;
    public $DataEntrada;
    public $DataSaida;
}

class Registrar{
    function RegistrarEntrada($entrada, $dataEntrada){
        global $Entrada;
        $Entrada = $entrada;
        $DataEntrada = $dataEntrada;
    }

    function RegistrarSaida($saida, $dataSaida){
        global $Saida;
        $Saida = $saida;
        $DataSaida = $dataSaida;
    }
}