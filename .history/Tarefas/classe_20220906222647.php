<?php

class Registro {

    public function __construct(
        public $Descricao,
        public $Valor,
        public $Data,
    ){}
    
}

class Registrar{

    public array $registros = [];

    public function __construct(){

    }

    /*function registrar(Registro $registro){
        $this->registros[] = $registro;
    }*/

    function registrar($descricao, $valor) {
        $registro = new Registro($descricao, $valor, new DateTime());
        $this->registros[] = $registro;
    }

}

$meusRegistros = new Registrar();
$seusRegistros = new Registrar();
//$registros->registrar(new Registro('teste', 10, null));
$registros->registrar('teste', 10);

var_dump($registros);