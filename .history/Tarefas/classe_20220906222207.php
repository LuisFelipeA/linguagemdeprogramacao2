<?php

class Registro {

    public function __construct(
        public $Descricao,
        public $Valor,
        public $Data,
    ){}
    
}

class Registrar{

    public array $registros = [];

    public function __construct(){

    }

    function registrar($entrada, $dataEntrada){
        global $Entrada;
        $Entrada = $entrada;
        $DataEntrada = $dataEntrada;
    }

    function RegistrarSaida($saida, $dataSaida){
        global $Saida;
        $Saida = $saida;
        $DataSaida = $dataSaida;
    }
}