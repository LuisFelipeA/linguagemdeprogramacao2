<?php

class Registro {

    public function __construct(
        public string $Descricao,
        public float $Valor,
        public DateTime $Data,
    ){}
    
}

class Registrar{

    public array $registros = [];

    /*function registrar(Registro $registro){
        $this->registros[] = $registro;
    }*/

    function registrar($descricao, $valor) {
        $registro = new Registro($descricao, $valor, new DateTime());
        $this->registros[] = $registro;
    }

}

$meusRegistros = new Registrar();
$seusRegistros = new Registrar();
//$registros->registrar(new Registro('teste', 10, null));
$meusRegistros->registrar('teste', 10);

var_dump($meusRegistros);
var_dump($seusRegistros);