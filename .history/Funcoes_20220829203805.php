<?php

global $Saldo;
$Saldo = 0;
global $Gastos;
$Gastos = 0;

function AdcionarSaldo($saldo){
    global $Saldo;
    $Saldo = $saldo;
}

function AdcionarGasto($gasto){
    global $Gastos;
    $Gastos += $gasto;
}

function CalcularSaldo(){
    global $Saldo;
    global $Gastos;
    $Valor = $Saldo - $Gastos;
    echo "O saldo Atual é: R$" . $Valor;
}

AdcionarSaldo(200.45);
AdcionarGasto(46);
CalcularSaldo();


