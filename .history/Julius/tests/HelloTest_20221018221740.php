<?php

// src/project-01/php/tests/HelloTest.php 

use PHPUnit\Framework\TestCase;
use Luisfelipe\Julius\Registrar;

final class HelloTest extends TestCase
{
    public function testHello() {
        $this->assertEquals("hello", "hello");
    }

    public function testRegistrar(){
        $meusRegistros = new Registrar();
        $seusRegistros = new Registrar();
        //$registros->registrar(new Registro('teste', 10, null));
        $meusRegistros->registrar('teste', 10);
        $meusRegistros->registrar('teste', 15);

        $this->assertEquals(0, $seusRegistros->balanco());
        $this->assertEquals(25, $meusRegistros->balanco());

    }
}


