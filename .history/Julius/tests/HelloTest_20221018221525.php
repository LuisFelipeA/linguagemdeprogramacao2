<?php

// src/project-01/php/tests/HelloTest.php 

use PHPUnit\Framework\TestCase;
use Luisfelipe\Julius\Registrar;

final class HelloTest extends TestCase
{
    public function testHello() {
        $this->assertEquals("hello", "hello");
    }

    public function testRegistrar(){
        
        $meusRegistros = new Registrar();
        $seusRegistros = new Registrar();
        //$registros->registrar(new Registro('teste', 10, null));
        $meusRegistros->registrar('teste', 10);
        $meusRegistros->registrar('teste', 15);

        var_dump($meusRegistros->balanco());
        var_dump($seusRegistros->balanco());
    }
}


