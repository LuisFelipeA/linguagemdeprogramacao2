<?php

namespace Luisfelipe\Julius;

class Registrar{

public array $registros = [];

/*function registrar(Registro $registro){
    $this->registros[] = $registro;
}*/

public function registrar($descricao, $valor) {
    $registro = new Registro($descricao, $valor, new DateTime());
    $this->registros[] = $registro;
}

public function balanco() : float {
    $balanco = 0;
    foreach($this->registros as $registro) {
        $balanco += $registro->Valor;
    }
    return $balanco;
}

}