<?php

namespace Luisfelipe\Julius;

class Registro {

    public function __construct(
        public string $Descricao,
        public float $Valor,
        public DateTime $Data,
    ){}
    
}