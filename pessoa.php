<?php

class Pessoa{
    public $nome;

    function __construct(string $nome){
        $this->nome = $nome;
    }

    function __toString(): string {
        return "Pessoas {nome: $this->nome }";
    }
}

$pessoa0 = new Pessoa('Luis');
$pessoa1 = new Pessoa('Felipe');

echo $pessoa0, ' ', $pessoa1, "\n";